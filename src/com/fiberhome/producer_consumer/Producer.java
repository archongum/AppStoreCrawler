package com.fiberhome.producer_consumer;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fiberhome.model.ContextData;

public class Producer implements Runnable {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	private Set<String> mSeenSet;
	private BlockingQueue<String> mUnseenQueue;
	private BufferedReader mEntranceBR;
	private BufferedReader mHandledBR;

	public Producer(ContextData contextData, BufferedReader entranceBR, BufferedReader handledBR) {
		this.mSeenSet = contextData.getSeenSet();
		this.mUnseenQueue = contextData.getUnseenQueue();
		this.mEntranceBR = entranceBR;
		this.mHandledBR = handledBR;
	}
	
	private void logic() throws IOException, InterruptedException {
		String suffix;
		
		// 加入爬虫入口。
		while ((suffix = mEntranceBR.readLine()) != null) {
			
			// 去空行。
			if (!suffix.isEmpty()) {
				mUnseenQueue.put(suffix);
			}
		}
		mEntranceBR.close();
		LOGGER.trace("Start: UnseenQueue=" + mUnseenQueue.size());
		
		// 如果第一次运行，mHandledBR会为空，直接结束该方法。
		if (mHandledBR == null) {
			LOGGER.trace("First Run.");
			return;
		}
		
		// 不是第一次运行，把已经访问过的标识加入mSeenSet中。
		while ((suffix = mHandledBR.readLine()) != null) {
			
			// 去空行。
			if (!suffix.isEmpty()) {
				mSeenSet.add(suffix);
			}
		}
		mHandledBR.close();
		LOGGER.trace("Start: Handled=" + mSeenSet.size());
	}

	@Override
	public void run() {
		try {
			logic();
		} catch (IOException e) {
			LOGGER.error("log4jException:", e);
		} catch (InterruptedException e) {
			LOGGER.error("log4jException:", e);
		}
	}
}
