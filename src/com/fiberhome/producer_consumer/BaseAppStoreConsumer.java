package com.fiberhome.producer_consumer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.fiberhome.model.AppInfo;
import com.fiberhome.model.ContextData;

public abstract class BaseAppStoreConsumer implements Runnable {
	
	private static final Logger LOGGER = LogManager.getLogger();

	private ContextData mContextData;

	private String mPrefix;
	private String mSelector;
	
	private BlockingQueue<String> mUnseenQueue;	
	
	public BaseAppStoreConsumer(ContextData contextData, String prefix, String selector) {
		this.mContextData = contextData;
		
		this.mPrefix = prefix;
		this.mSelector = selector;
		
		this.mUnseenQueue = contextData.getUnseenQueue();
	}

	@Override
	public void run() {
		while (!mContextData.isStop()) {
			try {
				logic();
				Thread.sleep(200);
			} catch (MalformedURLException e) {
				LOGGER.error("log4jException:", e);
			} catch (InterruptedException e) {
				LOGGER.error("log4jException:", e);
			}
		}
	}
	
	
	private void logic() throws MalformedURLException, InterruptedException {
		String suffix;
		if ((suffix = mUnseenQueue.poll(2, TimeUnit.SECONDS)) != null) {

			URL url =  new URL(mPrefix + suffix);
			Document doc = null;

			try {
				doc = Jsoup.parse(url, 5000);
			} catch (IOException e) {
				
				// Such as: 404.
				if (e.getMessage().contains("HTTP error fetching URL")) {
					return;
				} 

				// Timed out or reset.
				mContextData.putToUnseenQueue(suffix);
				LOGGER.error("log4jException:", e);
			}
			
			if (doc != null) {
				HashSet<String> newSuffixes = extractsSuffixes(doc, mSelector);
				for (String newsuffix : newSuffixes) {
					mContextData.putToUnseenQueue(newsuffix);
				}
				
				AppInfo info = getAppInfo(doc, suffix);
				mContextData.putToAppInfoQueue(info);
			}
		}
	}
	
	protected HashSet<String> extractsSuffixes(Document doc, String selector) {
		return null;
	}

	protected AppInfo getAppInfo(Document doc, String suffix) {
		return null;
	}
}
