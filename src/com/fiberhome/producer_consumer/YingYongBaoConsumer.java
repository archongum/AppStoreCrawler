package com.fiberhome.producer_consumer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.fiberhome.model.AppInfo;
import com.fiberhome.model.ContextData;

public class YingYongBaoConsumer extends BaseAppStoreConsumer {

	private static final String PREFIX = "http://android.myapp.com/myapp/detail.htm?apkName=";
	private static final String SELECTOR = "a[href*=detail.htm?apkName=]";

	public YingYongBaoConsumer(ContextData contextData) {
		super(contextData, PREFIX, SELECTOR);
	}
	
	@Override
	protected HashSet<String> extractsSuffixes(Document doc, String selector) {
		HashSet<String> suffixes = new HashSet<String>();

		Elements links = doc.select(selector);
		for (Element link : links) {
			String href = link.attr("href");
			String newSuffix = href.substring(href.indexOf("=") + 1, href.indexOf("&"));
			suffixes.add(newSuffix);
		}
		return suffixes;
	}



	@Override
	protected AppInfo getAppInfo(Document doc, String suffix) {
		
		// 名字。
		String appName = doc.select("div.det-name-int").text().trim();

		// 类型。
		String appType = doc.select("a.det-type-link").text().trim();

		// 大小。
		String appSize = doc.select("div.det-size").text().trim();

		// 描述。
		String appDesc = doc.select("div.det-intro-text").text();


		// 更新时间。
		// 得到的时间以秒为单位。
		long appUpdateLong = Long.parseLong(doc.select("div[class=det-othinfo-data]").attr("data-apkpublishtime")) * 1000;
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(appUpdateLong);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String appUpdate = format.format(calendar.getTime());

		// 版本号和作者。
		String appVersionAndAuthor = doc.select("div[class=det-othinfo-data]").text();
		String strings[] = appVersionAndAuthor.split("\\s\\s");

		String appVersion = "";
		String appAuthor = "";
		// 版本号。
		if (strings.length >= 1) {
			appVersion = strings[0].replace("V", "");
		}

		// 作者。
		if (strings.length >= 2) {
			appAuthor = strings[1];
		}

		
		return new AppInfo(appName, appType, appDesc, appVersion, appUpdate, appSize, appAuthor, suffix);
	}
}
