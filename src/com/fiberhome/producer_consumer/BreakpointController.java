package com.fiberhome.producer_consumer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fiberhome.model.ContextData;

public class BreakpointController implements Runnable {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	private ContextData mContextData;
	
	private BlockingQueue<String> mUnseenQueue;
	private File mBreakpointFile;
	
	public BreakpointController(ContextData contextData, File breakpointFile) {
		this.mContextData = contextData;

		this.mUnseenQueue = contextData.getUnseenQueue();
		this.mBreakpointFile = breakpointFile;
	}
	
	private void logic() throws FileNotFoundException {
		String suffix = null;
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mBreakpointFile)));
		try {
			for (int i=0; i<1000; i++) {
				if ((suffix = mUnseenQueue.peek()) != null) {
					bw.write(suffix + "\n");
					bw.flush();
					Thread.sleep(30);
				}
			}
		} catch (IOException e) {
			LOGGER.error("log4jException:", e);
		} catch (InterruptedException e) {
			LOGGER.error("log4jException:", e);
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				LOGGER.error("log4jException:", e);
			}
		}
	}

	@Override
	public void run() {
		while (!mContextData.isStop() && mUnseenQueue.size() > 100) {
			try {
				logic();
			} catch (FileNotFoundException e) {
				LOGGER.error("log4jException:", e);
			}
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				LOGGER.error("log4jException:", e);
			}
		}
	}
}
