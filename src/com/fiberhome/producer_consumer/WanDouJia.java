package com.fiberhome.producer_consumer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.fiberhome.model.AppInfo;
import com.fiberhome.model.ContextData;



public class WanDouJia extends BaseAppStoreConsumer {

	private static final String PREFIX = "http://www.wandoujia.com/apps/";
	private static final String SELECTOR = "a[href*=/apps/][data-track=detail-click-relateApp]";

	public WanDouJia(ContextData contextData) {
		super(contextData, PREFIX, SELECTOR);
	}
	
	@Override
	protected HashSet<String> extractsSuffixes(Document doc, String selector) {
		HashSet<String> suffixes = new HashSet<String>();

		Elements links = doc.select(selector);
		for (Element link : links) {
			String href = link.attr("href");
			String newSuffix = href.substring(30);
			suffixes.add(newSuffix);
		}
		return suffixes;
	}



	@SuppressWarnings("deprecation")
	@Override
	protected AppInfo getAppInfo(Document doc, String suffix) {
		
		// 名字。
		String appName = doc.select("span.title").text().trim();

		// 类型。
		String appType = doc.select("a[data-track=detail-click-appTag]").text().trim();

		// 大小。
		String appSize = "";
		String sizeString = doc.select("meta[itemprop=fileSize]").attr("content");
		if (!sizeString.isEmpty()) {
			float size = Float.parseFloat(sizeString) / 1048576;
			appSize = String.format("%.2fM", size);	
		}
		

		// 描述。
		String appDesc = doc.select("div[itemprop=description]").text();


		// 更新时间。
		// 得到日期，格式为：Jul 30, 2016。
		String appUpdate = "";
		String timeString = doc.select("time[itemprop=datePublished]").attr("datetime");
		if (!timeString.isEmpty()) {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			appUpdate = format.format(new Date(timeString));
		}
		

		// 版本号。
		String appVersion = "";
		Elements appInfosWithVersion = doc.select("dl.infos-list dd");
		if (appInfosWithVersion.size() >= 5) {
			appVersion = appInfosWithVersion.get(4).text().trim();
		}

		// 作者。
		String appAuthor = doc.select("a[itemprop=url] span[itemprop=name]").text().trim();
		
		return new AppInfo(appName, appType, appDesc, appVersion, appUpdate, appSize, appAuthor, suffix);
	}
}
