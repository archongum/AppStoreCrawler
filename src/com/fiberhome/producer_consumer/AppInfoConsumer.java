package com.fiberhome.producer_consumer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fiberhome.model.AppInfo;
import com.fiberhome.model.ContextData;

public class AppInfoConsumer implements Runnable {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	private ContextData mContextData;
	
	private Set<String> mSeenSet;
	private BlockingQueue<String> mUnseenQueue;
	private BlockingQueue<AppInfo> mAppInfoQueue;
	
	private BufferedWriter mAppinfoFileBW;
	private BufferedWriter mHandledFileBW;
	
	private int handledCount = 0;
	private String startTime;
	
	public AppInfoConsumer(ContextData contextData,
			BufferedWriter appinfoFileBW, BufferedWriter handledFileBW) {
		this.mContextData = contextData;

		this.mSeenSet = contextData.getSeenSet();
		this.mUnseenQueue = contextData.getUnseenQueue();
		this.mAppInfoQueue = contextData.getAppInfoQueue();
		
		this.mAppinfoFileBW = appinfoFileBW;
		this.mHandledFileBW = handledFileBW;

		this.handledCount = mSeenSet.size();
	}

	@Override
	public void run() {

		startTime = getTime();

		while (!mUnseenQueue.isEmpty()) {
			try {
				logic();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		mContextData.setStop(true);

		LOGGER.trace("Start Time=" + startTime + "\nEnd Time=" + getTime());
	}
	
	private void logic() throws InterruptedException, IOException {
		AppInfo info;
		while ((info = mAppInfoQueue.poll(30, TimeUnit.SECONDS) ) != null) {
			String suffix = info.getAppPackage();
			if (suffix == null || suffix.isEmpty()) {
				return;
			}
			if (!mSeenSet.contains(suffix)) {
				mSeenSet.add(suffix);
				mAppinfoFileBW.write(format(info));
				mAppinfoFileBW.flush();
				mHandledFileBW.write(info.getAppPackage() + "\n");
				mHandledFileBW.flush();
				handledCount++;
				LOGGER.trace("Handled=" + handledCount + ", Unseen=" + mUnseenQueue.size());
			}
		}
	}
	
	private String format(AppInfo info) {
		StringBuilder sb = new StringBuilder();
		sb.append(info.getAppName())
		  .append("\t")
		  .append(info.getAppType())
		  .append("\t")
		  .append(info.getAppPackage())
		  .append("\t")
		  .append(info.getAppVersion())
		  .append("\t")
		  .append(info.getAppSize())
		  .append("\t")
		  .append(info.getAppUpdate())
		  .append("\t")
		  .append(info.getAppAuthor())
		  .append("\t")
		  .append(info.getAppDesc())
		  .append("\n");
		return sb.toString();
	}
	
	private String getTime() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return format.format(Calendar.getInstance().getTime());
	}
}
