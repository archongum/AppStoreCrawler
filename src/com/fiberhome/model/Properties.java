package com.fiberhome.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Properties {
	
	public static final String VERSION_EXTRACT = "1";
	
	public static final String VERSION_ENUM = "2";
	
	private String mVersion;
	
	public Properties() {
		java.util.Properties p = new java.util.Properties();
		File file = new File("config/config.properties");
		try {
			p.load(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
