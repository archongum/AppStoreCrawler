package com.fiberhome.model;

public class AppInfo {

	private String appId;
	
	private String appName;

	private String appType;
	
	private String appDesc;
	
	private String appVersion;
	
	private String appUpdate;

	private String appSize;

	private String appAuthor;

	private String appPackage;
	

	public AppInfo() {}

	public AppInfo(String appName, String appType, String appDesc,
			String appVersion, String appUpdate, String appSize,
			String appAuthor, String appPackage) {
		this.appName = appName;
		this.appType = appType;
		this.appDesc = appDesc;
		this.appVersion = appVersion;
		this.appUpdate = appUpdate;
		this.appSize = appSize;
		this.appAuthor = appAuthor;
		this.appPackage = appPackage;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppDesc() {
		return appDesc;
	}

	public void setAppDesc(String appDesc) {
		this.appDesc = appDesc;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppUpdate() {
		return appUpdate;
	}

	public void setAppUpdate(String appUpdate) {
		this.appUpdate = appUpdate;
	}

	public String getAppSize() {
		return appSize;
	}

	public void setAppSize(String appSize) {
		this.appSize = appSize;
	}

	public String getAppAuthor() {
		return appAuthor;
	}

	public void setAppAuthor(String appAuthor) {
		this.appAuthor = appAuthor;
	}

	public String getAppPackage() {
		return appPackage;
	}

	public void setAppPackage(String appPackage) {
		this.appPackage = appPackage;
	}

	@Override
	public String toString() {
		return "AppInfo [appName=" + appName
				+ ", appType=" + appType + ", appDesc=" + appDesc
				+ ", appVersion=" + appVersion + ", appUpdate=" + appUpdate
				+ ", appSize=" + appSize + ", appAuthor=" + appAuthor
				+ ", appPackage=" + appPackage + "]";
	}
}
