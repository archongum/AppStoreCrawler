package com.fiberhome.model;

import java.util.Set;
import java.util.concurrent.BlockingQueue;


public class ContextData {
	
	private Set<String> mSeenSet;
	private BlockingQueue<String> mUnseenQueue;
	private BlockingQueue<AppInfo> mAppInfoQueue;
	
	private boolean isStop = false;
	
	
	public ContextData(Set<String> seenSet, BlockingQueue<String> unseenQueue,
			BlockingQueue<AppInfo> appInfoQueue) {
		this.mSeenSet = seenSet;
		this.mUnseenQueue = unseenQueue;
		this.mAppInfoQueue = appInfoQueue;
	}
	
	public Set<String> getSeenSet() {
		return mSeenSet;
	}
	
	public BlockingQueue<String> getUnseenQueue() {
		return mUnseenQueue;
	}
	
	public BlockingQueue<AppInfo> getAppInfoQueue() {
		return mAppInfoQueue;
	}
	
	public void putToUnseenQueue(String suffix) throws InterruptedException {
		
		// 过滤已经访问过的，以及已经在未访问队列里面的。
		if (suffix == null || suffix.isEmpty() || mSeenSet.contains(suffix) || mUnseenQueue.contains(suffix)) {
			return;
		}

		mUnseenQueue.put(suffix);
	}
	
	public void putToAppInfoQueue(AppInfo info) throws InterruptedException {
		
		// 过滤无应用名，或者无包名（或标识），或者无类型的应用。
		if (info.getAppName().isEmpty() || info.getAppPackage().isEmpty() || info.getAppType().isEmpty()) {
			return;
		}
		
		mAppInfoQueue.put(info);
	}
	

	public boolean isStop() {
		return isStop;
	}

	public void setStop(boolean isStop) {
		this.isStop = isStop;
	}
}
