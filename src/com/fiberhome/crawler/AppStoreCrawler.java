package com.fiberhome.crawler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import com.fiberhome.model.AppInfo;
import com.fiberhome.model.ContextData;
import com.fiberhome.producer_consumer.AppInfoConsumer;
import com.fiberhome.producer_consumer.BreakpointController;
import com.fiberhome.producer_consumer.Producer;
import com.fiberhome.producer_consumer.YingYongBaoConsumer;

public class AppStoreCrawler {
	
	private static final int THREAD_COUNT = 100;
	
	private static final String HANDLED_FILE = "D:\\app\\handled.txt";
	private static final String ENTRANCE_FILE = "D:\\app\\entrance.txt";
	private static final String APPINFO_FILE = "D:\\app\\appinfo.txt";
	private static final String BREAKPOINT_FILE = "D:\\app\\breakpoint.txt";
	
	private ContextData mContextData;
	
	private void crawls() {

		Set<String> seenSet = new HashSet<String>();
		BlockingQueue<String> unseenQueue = new LinkedBlockingDeque<String>();
		BlockingQueue<AppInfo> appInfoQueue = new LinkedBlockingDeque<AppInfo>();
		
		mContextData = new ContextData(seenSet, unseenQueue, appInfoQueue);
		
		// 生产者：加入断点文件里的标识，以及未处理文件里的标识。
		File entranceFile = null;
		File handledFile = new File(HANDLED_FILE);
		File breakpointFile = new File(BREAKPOINT_FILE);

		if (breakpointFile.exists()) {
			entranceFile = breakpointFile;
		} else {
			entranceFile = new File(ENTRANCE_FILE);
		}
		
		if (!entranceFile.exists()) {
			System.out.println("Entrance File doesn't exist");
			return;
		}

		BufferedReader entranceBR = null;
		BufferedReader handledBR = null;
		try {
			entranceBR = new BufferedReader(new InputStreamReader(new FileInputStream(entranceFile)));
			if (handledFile.exists()) {
				handledBR = new BufferedReader(new InputStreamReader(new FileInputStream(handledFile)));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (entranceBR != null) {
			new Thread(new Producer(mContextData, entranceBR, handledBR)).start();
		}
		
		// 给5秒时间生产者“生产”。
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		// 消费者：爬取信息。
		for (int i = 0; i < THREAD_COUNT; i++) {
			new Thread(new YingYongBaoConsumer(mContextData)).start();
		}
		
		// 给5秒时间消费者爬取信息。
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
				
		// 消费者：写出信息。
		File appInfoFile = new File(APPINFO_FILE);
		BufferedWriter appinfoFileBW = null;
		BufferedWriter handledFileBW = null;
		try {
			appinfoFileBW = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(appInfoFile, true)));
			handledFileBW = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(handledFile, true)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (appinfoFileBW != null && handledFileBW != null) {
			new Thread(new AppInfoConsumer(mContextData, appinfoFileBW, handledFileBW)).start();
		}

		// 断点管理者。
		new Thread(new BreakpointController(mContextData, breakpointFile)).start();
	}

	public static void main(String[] args) {
		new AppStoreCrawler().crawls();
	}
}
